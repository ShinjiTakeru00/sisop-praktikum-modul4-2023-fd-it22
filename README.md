# sisop-praktikum-modul4-2023-FD-IT22



## Anggota Kelompok IT22

Evan Darya Kusuma (5027211069)

## Nomor 1

**Soal**

Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.



Catatan: 
Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
Perhatikan port  pada masing-masing instance.

**Code**
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void executeCommand(char *command[], char *commandName) {
    pid_t pid;
    int exit_status;

    if ((pid = fork()) < 0) {
        printf("Failed to fork a process.\n");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {  // child process
        execvp(commandName, command);
        printf("Failed to execute %s command.\n", commandName);
        exit(EXIT_FAILURE);
    } else {  // parent process
        waitpid(pid, &exit_status, 0);
    }
}

int main() {
    char *downloadCommand[] = {"kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL};
    char *unzipCommand[] = {"unzip", "fifa-player-stats-database.zip", NULL};

    executeCommand(downloadCommand, "kaggle");

    executeCommand(unzipCommand, "unzip");

    system("awk -F\",\" '{if ($3 < 25 && $8 > 85 && $9 != \"Manchester City\") print \"Name :\",$2,\"\\n Club: \",$9,\"\\n Age: \",$3,\"\\n Potential: \",$8,\"\\n Photo: \",$4,\"\\n Nationality: \",$5,\"\\n\"}' FIFA23_official_data.csv");

    return 0;
}

```

## Nomor 3

**Soal**

Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.
Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c. 
Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).
Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).
Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. 
Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun. 
Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂

Catatan: 
Pastikan dalam FUSE tersebut (baik yang normal dari /etc maupun yang telah dimodifikasi) dapat melakukan operasi mkdir, cat dan mv.
Pastikan source mount kalian dari .zip yang udah dikasih (inifolderetc/sisop) dan JANGAN PAKAI /etc ASLI KALIAN! kerusakan Linux bukan tanggung jawab kami.


**Code**
```
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

const char *base_path = "/home/shinji/PraktikumSisop/modul4/soal3/etc/sisop";
const char *secret_key = "Evan@2008";

static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeBase64File(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return;
    }

    // Get file size
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Allocate memory for file content
    uint8_t *file_content = (uint8_t *)malloc(file_size);
    if (file_content == NULL) {
        printf("Memory allocation failed\n");
        fclose(file);
        return;
    }

    // Read file content
    size_t bytes_read = fread(file_content, 1, file_size, file);
    fclose(file);

    if (bytes_read != file_size) {
        printf("Failed to read file: %s\n", filename);
        free(file_content);
        return;
    }

    // Encode file content as Base64
    uint8_t input[3], output[4];
    size_t input_len = 0;
    size_t output_len = 0;
    size_t i = 0;
    
    file = fopen(filename, "wb");
    if (file == NULL) {
        printf("Failed to open file for writing: %s\n", filename);
        free(file_content);
        return;
    }

    while (i < file_size) {
        input[input_len++] = file_content[i++];
        if (input_len == 3) {
            output[0] = base64_table[input[0] >> 2];
            output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
            output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
            output[3] = base64_table[input[2] & 0x3F];

            fwrite(output, 1, 4, file);

            input_len = 0;
            output_len += 4;
        }
    }

    if (input_len > 0) {
        for (size_t j = input_len; j < 3; j++) {
            input[j] = 0;
        }

        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, input_len + 1, file);

        for (size_t j = input_len + 1; j < 4; j++) {
            fputc('=', file);
        }

        output_len += 4;
    }

    fclose(file);
    free(file_content);

    printf("File encoded successfully. file: %s\n", filename);
}

bool is_encoded(const char *name) {
    char first_char = name[0];
    if (first_char == 'l' || first_char == 'L' ||
        first_char == 'u' || first_char == 'U' ||
        first_char == 't' || first_char == 'T' ||
        first_char == 'h' || first_char == 'H') {
        return true;
    }
    return false;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

void modify_filename(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = tolower(name[i]);
    }
}

void modify_directoryname(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = toupper(name[i]);
    }
}

void convert_to_binary(char *name)
{
    int len = strlen(name);
    char *temp = (char *)malloc((8 * len + len) * sizeof(char)); // Allocate memory for modified string

    int index = 0; // Index for the modified string

    for (int i = 0; i < len; i++)
    {
        char ch = name[i];

        // Convert character to binary string
        for (int j = 7; j >= 0; j--)
        {
            temp[index++] = (ch & 1) + '0'; // Convert bit to character '0' or '1'
            ch >>= 1;                       // Shift right by 1 bit
        }

        if (i != len - 1)
        {
            temp[index++] = ' '; // Add space separator between binary codes
        }
    }

    temp[index] = '\0'; // Null-terminate the modified string

    strcpy(name, temp); // Update the original string with the modified string
    free(temp);         // Free the allocated memory
}

void changePath(char *str1, char *str2) {
    char *slash = strrchr(str1, '/');
    char temp[100];
    strcpy(temp, str1);
    if (slash != NULL) {
        ++slash; // Move past the slash
        strcpy(slash, str2);
        rename(temp, str1);
    }
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        char name[100];
        strcpy(name, de->d_name);
        char cur_path[1500];
        sprintf(cur_path, "%s/%s", fpath, de->d_name);

        res = (filler(buf, name, &st, 0));

        if (res != 0)
            break;
        if(de->d_type == DT_REG) {
            if(is_encoded(name)) {
                encodeBase64File(cur_path);
            }
            else if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_filename(name);
            }
            changePath(cur_path, name);
        }
        else {
            if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_directoryname(name);
            }
            changePath(cur_path, name);
            xmp_readdir(cur_path, buf, filler, offset, fi);
        }
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char input_password[100];
            printf("Masukkan password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0) {
                printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Modify the directory name to all uppercase
    char modified_name[256];
    strcpy(modified_name, path);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    // Create the directory with the modified name
    int res = mkdir(fpath, mode);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    char fpath_from[1000];
    char fpath_to[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);

    // Modify the destination name to all uppercase
    char modified_name[256];
    strcpy(modified_name, to);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    // Rename the file/directory with the modified name
    int res = rename(fpath_from, fpath_to);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .open = xmp_open,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
};

int main(int argc, char *argv[])
{
    umask(0);

    // Run the FUSE filesystem
    int fuse_stat = fuse_main(argc, argv, &xmp_oper, NULL);

    return fuse_stat;
}

```

## Nomor 4

**Soal**

Pada suatu masa, terdapat sebuah perusahaan bernama Bank Sabar Indonesia yang berada pada masa kejayaannya. Bank tersebut memiliki banyak sekali kegiatan-kegiatan yang  krusial, seperti mengolah data nasabah yang mana berhubungan dengan uang. Suatu ketika, karena susahnya maintenance, petinggi bank ini menyewa seseorang yang mampu mengorganisir file-file yang ada di Bank Sabar Indonesia. 
Salah satu karyawan di bank tersebut merekomendasikan Bagong sebagai seseorang yang mampu menyelesaikan permasalahan tersebut. Bagong memikirkan cara terbaik untuk mengorganisir data-data nasabah dengan cara membagi file-file yang ada dalam bentuk modular, yaitu membagi file yang mulanya berukuran besar menjadi file-file chunk yang berukuran kecil. Hal ini bertujuan agar saat terjadi error, Bagong dapat mudah mendeteksinya. Selain dari itu, agar Bagong mengetahui setiap kegiatan yang ada di filesystem, Bagong membuat sebuah sistem log untuk mempermudah monitoring kegiatan di filesystem yang mana, nantinya setiap kegiatan yang terjadi akan dicatat di sebuah file log dengan ketentuan sebagai berikut. 
Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
Format untuk logging yaitu sebagai berikut.
[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]

Contoh:
REPORT::230419-12:29:28::RENAME::/bsi23/bagong.jpg::/bsi23/bagong.jpeg
REPORT::230419-12:29:33::CREATE::/bsi23/bagong.jpg
FLAG::230419-12:29:33::RMDIR::/bsi23

Tidak hanya itu, Bagong juga berpikir tentang beberapa hal yang berkaitan dengan ide modularisasinya sebagai berikut yang ditulis dalam modular.c.
Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 
Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.
Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.
Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
Contoh:
file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).
Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).


**Code**
```
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/wait.h>
#define CHUNK 1024 

static  const  char * dirPath = "/home/shinji/PraktikumSisop/Modul4/soal4";

char kode[10] = "module_";

void logSystem(char* c, int type){
    FILE * logFile = fopen("/home/shinji/PraktikumSisop/Modul4/soal4fs_module.log", "a");
	time_t currTime;
	struct tm * time_info;
	time ( &currTime );
	time_info = localtime (&currTime);
    int yr=time_info->tm_year,mon=time_info->tm_mon,day=time_info->tm_mday,hour=time_info->tm_hour,min=time_info->tm_min,sec=time_info->tm_sec;
    if(type==1){//report type
        fprintf(logFile, "REPORT::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    else if(type==2){ //flag type
        fprintf(logFile, "FLAG::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    fclose(logFile);
}

int segment_exists(const char* filename) {
    FILE* file = fopen(filename, "rb");
    if (file == NULL) {
        return 0;  // File does not exist
    }
    fclose(file);
    return 1;  // File exists
}

long file_size(char *name){
    FILE *fp = fopen(name, "rb"); //must be binary read to get bytes
    // printf("%s\n",name);
    long size=-1;
    if(fp)
    {
        fseek (fp, 0, SEEK_END);
        size = ftell(fp)+1;
        fclose(fp);
    }
    return size;
}

void decode(const char *directoryPath, const char *prefix) {
    DIR *dir;
    struct dirent *entry;
    char filePath[1000];
    char mergedFilePath[1000];
    FILE *mergedFile;
    size_t prefixLength = strlen(prefix);

    dir = opendir(directoryPath);

    sprintf(mergedFilePath, "%s/%s", directoryPath, prefix);
    mergedFile = fopen(mergedFilePath, "w");

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strncmp(entry->d_name, prefix, prefixLength) == 0) {
            sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
            FILE *file = fopen(filePath, "r");

			// Skip the merged file itself
            if (strcmp(entry->d_name, prefix) == 0) {
                fclose(file);
                continue;
            }

            char buffer[1024];
            size_t bytesRead;

            while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
                fwrite(buffer, 1, bytesRead, mergedFile);
            }

            fclose(file);
        } 

    }

    closedir(dir);
    fclose(mergedFile);

    dir = opendir(directoryPath);

	//remove useless file
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            if (strncmp(entry->d_name, prefix, prefixLength) == 0) {
                char extension[1000];
                sprintf(extension, ".%03d", atoi(entry->d_name + prefixLength + 1));
                if (strstr(entry->d_name, extension) != NULL) {
                    sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
                    if (strcmp(filePath, mergedFilePath) != 0) {
                        remove(filePath);
                    }
                }
            }
        }
    }

    closedir(dir);
}

void encrypt(char* enc1, char * enc2){
    if(strcmp(enc1, ".") == 0) return;
    if(strcmp(enc1, "..") == 0)return;
    
    int chunks=0, i, accum;

    char largeFileName[200];    //change to your path
    sprintf(largeFileName,"%s/%s/%s",dirPath,enc2,enc1);
    printf("%s\n",largeFileName);

    char filename[260];//base name for small files.
    sprintf(filename,"%s.",largeFileName);
    //printf("%s\n",filename);

    char smallFileName[300];
    char line[1080];
    FILE *fp1, *fp2;
    long sizeFile = file_size(largeFileName);
    // printf("File size : %ld\n",sizeFile);
    
	if (sizeFile > 1025){
		chunks = sizeFile/CHUNK + 1;//ensure end of file
		// printf("%d",chunks);
		fp1 = fopen(largeFileName, "r");
		if(fp1)
		{
			char number[5];
			printf("Splitting\n");
			for(i=0;i<chunks;i++)
			{
				accum = 0;
				sprintf(number,"%03d",i+1);
				
				sprintf(smallFileName, "%s%s", filename, number);

				// Check if segment file already exists, skip if it does
				if (segment_exists(smallFileName)) {
					continue;
				}
				
				fp2 = fopen(smallFileName, "wb");
				if (fp2) {
					while (accum < CHUNK && !feof(fp1)) {
						size_t bytesRead = fread(line, 1, sizeof(line), fp1);
						size_t bytesToWrite = bytesRead;
						if (accum + bytesToWrite > CHUNK) {
							bytesToWrite = CHUNK - accum;
						}
						fwrite(line, 1, bytesToWrite, fp2);
						accum += bytesToWrite;
					}
					fclose(fp2);
				}
			}
			fclose(fp1);
			// Hapus file asli setelah pemecahan selesai
			if (remove(largeFileName) != 0) {
				printf("Gagal menghapus file asli: %s\n", largeFileName);
			} else {
				printf("File asli dihapus: %s\n", largeFileName);
			}
		}
	}
    printf("keluar\n");
}

void filter(char * enc1){
    if(strcmp(enc1, ".") == 0) return;
    if(strcmp(enc1, "..") == 0)return;
	if(strstr(enc1, "/") == NULL)return;
}

static  int  xmp_getattr(const char *path, struct stat *stbuf){
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }
	char newPath[1000];
	int res;
	sprintf(newPath,"%s%s", dirPath, path);
	res = lstat(newPath, stbuf);
	if (res == -1)
		return -errno;
	return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){ //baca directory
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);

	int res = 0;
	struct dirent *dir;
	DIR *dp;
	(void) fi;
	(void) offset;
	dp = opendir(newPath);
	if (dp == NULL) return -errno;

	while ((dir = readdir(dp)) != NULL) { //buat loop yang ada di dalem directory
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if(enc2 != NULL){
			encrypt(dir->d_name, enc2);
        } else {
			char *extension = strrchr(dir->d_name, '.');
            if (extension != NULL && strcmp(extension, ".001") == 0) {
                *extension = '\0';
                decode(newPath, dir->d_name);
            }
		}
		res = (filler(buf, dir->d_name, &st, 0));
		if(res!=0) break;
	}

	closedir(dp);
	return 0;
}

static int xmp_mkdir(const char *path, mode_t mode){ //buat bikin directory baru

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	}
	else sprintf(newPath, "%s%s",dirPath,path);

	int res = mkdir(newPath, mode);
    char str[100];
	sprintf(str, "MKDIR::%s", path);
	logSystem(str,1);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev){//buat file

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path = dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);
	int res;

	if (S_ISREG(mode)) {
		res = open(newPath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (res >= 0)
			res = close(res);
	} else if (S_ISFIFO(mode))
		res = mkfifo(newPath, mode);
	else
		res = mknod(newPath, mode, rdev);
    char str[100];
	sprintf(str, "CREATE::%s", path);
	logSystem(str,1);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_unlink(const char *path) { //ngehapus file
	printf("unlink\n");
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);
    char str[100];
	sprintf(str, "REMOVE::%s", path);
	logSystem(str,2);
	int res;
	res = unlink(newPath);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rmdir(const char *path) {//ngehapus directory
	printf("rmdir\n");
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

	char newPath[1000];
	sprintf(newPath, "%s%s",dirPath,path);
    char str[100];
	sprintf(str, "RMDIR::%s", path);
	logSystem(str,2);
	int res;
	res = rmdir(newPath);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rename(const char *from, const char *to) { //buat renme
	printf("\nRENAME!!!\n");
	char fileFrom[1000],fileTo[1000];
	sprintf(fileFrom,"%s%s",dirPath,from);
	sprintf(fileTo,"%s%s",dirPath,to);

    char str[100];
	sprintf(str, "RENAME::%s::%s", from, to);
	logSystem(str,1);
	int res;
	res = rename(fileFrom, fileTo);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi){ //open file
	char newPath[1000];
	sprintf(newPath, "%s%s",dirPath,path);
	int res;
	res = open(newPath, fi->flags);
	if (res == -1)
		return -errno;
	close(res);
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){ //read file
	
	char newPath[1000];
	sprintf(newPath, "%s%s",dirPath,path);
	int fd;
	int res;

	(void) fi;
	fd = open(newPath, O_RDONLY);
	if (fd == -1)
		return -errno;
	res = pread(fd, buf, size, offset);
	if (res == -1)
		res = -errno;
	close(fd);
	return res;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) { //write file
	
	char newPath[1000];
	sprintf(newPath, "%s%s", dirPath, path);
	
    int fd;
	int res;
	(void) fi;
	fd = open(newPath, O_WRONLY);
	if (fd == -1)
		return -errno;
    char str[100];
	sprintf(str, "WRITE::%s", path);
	logSystem(str,1);
	res = pwrite(fd, buf, size, offset);
	if (res == -1)
		res = -errno;
	close(fd);
	return res;
}

static struct fuse_operations xmp_oper = {

	.getattr = xmp_getattr,
	.readdir = xmp_readdir,
	.read = xmp_read,
	.mkdir = xmp_mkdir,
	.mknod = xmp_mknod,
	.unlink = xmp_unlink,
	.rmdir = xmp_rmdir,
	.rename = xmp_rename,
	.open = xmp_open,
	.write = xmp_write,

};

int  main(int  argc, char *argv[]){
	umask(0);

	return fuse_main(argc, argv, &xmp_oper, NULL);
}

```
