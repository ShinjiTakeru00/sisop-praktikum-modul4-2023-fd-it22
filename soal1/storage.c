#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void executeCommand(char *command[], char *commandName) {
    pid_t pid;
    int exit_status;

    if ((pid = fork()) < 0) {
        printf("Failed to fork a process.\n");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {  // child process
        execvp(commandName, command);
        printf("Failed to execute %s command.\n", commandName);
        exit(EXIT_FAILURE);
    } else {  // parent process
        waitpid(pid, &exit_status, 0);
    }
}

int main() {
    char *downloadCommand[] = {"kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL};
    char *unzipCommand[] = {"unzip", "fifa-player-stats-database.zip", NULL};

    executeCommand(downloadCommand, "kaggle");

    executeCommand(unzipCommand, "unzip");

    system("awk -F\",\" '{if ($3 < 25 && $8 > 85 && $9 != \"Manchester City\") print \"Name :\",$2,\"\\n Club: \",$9,\"\\n Age: \",$3,\"\\n Potential: \",$8,\"\\n Photo: \",$4,\"\\n Nationality: \",$5,\"\\n\"}' FIFA23_official_data.csv");

    return 0;
}
